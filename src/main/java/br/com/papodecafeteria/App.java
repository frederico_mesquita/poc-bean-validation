package br.com.papodecafeteria;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import br.com.papodecafeteria.domain.Address;
import br.com.papodecafeteria.domain.CNAE;
import br.com.papodecafeteria.domain.CNPJ;
import br.com.papodecafeteria.domain.ContactInformation;
import br.com.papodecafeteria.domain.Dimensoes;
import br.com.papodecafeteria.domain.NaturezaJuridicaCNPJ;
import br.com.papodecafeteria.domain.Phone;
import br.com.papodecafeteria.domain.Product;
import br.com.papodecafeteria.domain.ProductCategory;

public class App {
    public static void main( String[] args ){
        System.out.println( "Hello Bean Validation!" );
        try{
        	getOk();
        	getError1();
        	getError2();
        	getErrorGeral();
        }catch (Exception e) {
			System.out.println(e.getMessage());
		}
    }
    
    private static void getOk(){
    	System.out.println( "\n\nExecu��o sem erros!" );
        try{
        	CNPJ suplier = CNPJ.getCNPJ(0, (new Date()), "Empresa Brasileira de Produtos S/A", "Xulambs da Serra", 
        							Arrays.asList(
        								CNAE.getCNAE(0, "2543-8/00", "ALAVANCAS E P�S-DE-CABRA; FABRICA��O DE"),
        								CNAE.getCNAE(0, "2543-8/00", "ANCINHOS MANUAIS; FABRICA��O DE")), 
        							Arrays.asList(
        								CNAE.getCNAE(0, "2543-8/00", "ALAVANCAS E P�S-DE-CABRA; FABRICA��O DE"),
        								CNAE.getCNAE(0, "2543-8/00", "ANCINHOS MANUAIS; FABRICA��O DE")), 
        							NaturezaJuridicaCNPJ.getNaturezaJuridicaCNPJ(
        								0, "209-7", "Sociedade Empres�ria em Comandita por A��es", "Diretor ou Presidente", "16"), 
        							Arrays.asList(
        								ContactInformation.getContactInformation(
        									0, 
        									Address.getAddress(0, "Nome da rua", "8", "00000-000", ""), 
        									Phone.getPhone(0, 11, "00000-0000"), 
        									"")
        							), 
        							"00000000/0000-00");
        	
            Product pProduct = Product.getProduct(
            					0, 
            					"Nome do Produto", 
            					"Descri��o do produto", 
            					Dimensoes.getDimensoes(
            								2.45, 10.0, 7.0, "cm", 
            								90.0, 0.10, "g"), 
            					new ArrayList(Arrays.asList("ANS-011 Norma sanit�ria")), 
            					new ArrayList(Arrays.asList("path/img.ext")), 
            					"Composi��o do produto", 
            					ProductCategory.getProductCategory(
            										0, 
            										"abc", 
            										"Descri��o da categoria do produto")
            					, Arrays.asList(suplier));	
            System.out.println(pProduct.toString());
        }catch (Exception e) {
			System.out.println(e.getMessage());
		}
    }
    
    private static void getError1(){
    	System.out.println( "\n\nExecu��o com erros no CNPJ!" );
        try{
        	CNPJ suplier = CNPJ.getCNPJ(0, (new Date()), "Empresa Brasileira de Produtos S/A", "Xulambs da Serra", 
        							Arrays.asList(
        								CNAE.getCNAE(0, "2543-8/0", "ALAVANCAS E P�S-DE-CABRA; FABRICA��O DE"),
        								CNAE.getCNAE(0, "2543-8/00", "ANCINHOS MANUAIS; FABRICA��O DE")), 
        							Arrays.asList(
        								CNAE.getCNAE(0, "2543-8/00", "ALAVANCAS E P�S-DE-CABRA; FABRICA��O DE"),
        								CNAE.getCNAE(0, "2543-8/00", "ANCINHOS MANUAIS; FABRICA��O DE")), 
        							NaturezaJuridicaCNPJ.getNaturezaJuridicaCNPJ(
        								0, "209-7", "Sociedade Empres�ria em Comandita por A��es", "Diretor ou Presidente", "16"), 
        							Arrays.asList(
        								ContactInformation.getContactInformation(
        									0, 
        									Address.getAddress(0, "Nome da rua", "", "00000-000", ""), 
        									Phone.getPhone(0, 11, "0000-0000"), 
        									"")
        							), 
        							"0000000/0000-00");
        	
            Product pProduct = Product.getProduct(
            					0, 
            					"Nome do Produto", 
            					"Descri��o do produto", 
            					Dimensoes.getDimensoes(
            								2.45, 10.0, 7.0, "cm", 
            								90.0, 0.10, "g"), 
            					new ArrayList(Arrays.asList("ANS-011 Norma sanit�ria")), 
            					new ArrayList(Arrays.asList("path/img.ext")), 
            					"Composi��o do produto", 
            					ProductCategory.getProductCategory(
            										0, 
            										"abc", 
            										"Descri��o da categoria do produto")
            					, Arrays.asList(suplier));	
            System.out.println(pProduct.toString());
        }catch (Exception e) {
			System.out.println(e.getMessage());
		}
    }
    
    private static void getError2(){
    	System.out.println( "\n\nExecu��o com erros no CNPJ e no produto!" );
        try{
        	CNPJ suplier = CNPJ.getCNPJ(0, (new Date()), "Empresa Brasileira de Produtos S/A", "Xulambs da Serra", 
        							Arrays.asList(
        								CNAE.getCNAE(0, "2543-8/0", "ALAVANCAS E P�S-DE-CABRA; FABRICA��O DE"),
        								CNAE.getCNAE(0, "2543-8/00", "ANCINHOS MANUAIS; FABRICA��O DE")), 
        							Arrays.asList(
        								CNAE.getCNAE(0, "2543-8/00", "ALAVANCAS E P�S-DE-CABRA; FABRICA��O DE"),
        								CNAE.getCNAE(0, "2543-8/00", "ANCINHOS MANUAIS; FABRICA��O DE")), 
        							NaturezaJuridicaCNPJ.getNaturezaJuridicaCNPJ(
        								0, "209-7", "Sociedade Empres�ria em Comandita por A��es", "Diretor ou Presidente", "16"), 
        							Arrays.asList(
        								ContactInformation.getContactInformation(
        									0, 
        									Address.getAddress(0, "Nome da rua", "", "00000-000", ""), 
        									Phone.getPhone(0, 11, "0000-0000"), 
        									"")
        							), 
        							"0000000/0000-00");
        	
            Product pProduct = Product.getProduct(
            					0, 
            					"", 
            					"Descri��o do produto", 
            					Dimensoes.getDimensoes(
            								2.45, 10.0, 7.0, "cm", 
            								90.0, 0.10, "azsxdcfvgbh"), 
            					new ArrayList(Arrays.asList("ANS-011 Norma sanit�ria")), 
            					new ArrayList(Arrays.asList("path/img.ext")), 
            					"Composi��o do produto", 
            					ProductCategory.getProductCategory(
            										0, 
            										"abcd", 
            										"Descri��o da categoria do produto")
            					, Arrays.asList(suplier));	
            System.out.println(pProduct.toString());
        }catch (Exception e) {
			System.out.println(e.getMessage());
		}
    }
    
    private static void getErrorGeral(){
    	System.out.println( "\n\nExecu��o com erros no CNPJ e no produto!" );
        try{
        	CNPJ suplier = CNPJ.getCNPJ(0, (new Date()), "Empresa Brasileira de Produtos S/A", "Xulambs da Serra", 
        							null, 
        							Arrays.asList(
        								CNAE.getCNAE(0, "2543-8/00", "ALAVANCAS E P�S-DE-CABRA; FABRICA��O DE"),
        								CNAE.getCNAE(0, "2543-8/00", "ANCINHOS MANUAIS; FABRICA��O DE")), 
        							NaturezaJuridicaCNPJ.getNaturezaJuridicaCNPJ(
        								0, "209-7", "Sociedade Empres�ria em Comandita por A��es", "Diretor ou Presidente", "16"), 
        							Arrays.asList(
        								ContactInformation.getContactInformation(
        									0, 
        									Address.getAddress(0, "Nome da rua", "", "00000-000", ""), 
        									Phone.getPhone(0, 11, "0000-0000"), 
        									"")
        							), 
        							null);
        	
            Product pProduct = Product.getProduct(
            					0, 
            					"", 
            					"Descri��o do produto", 
            					Dimensoes.getDimensoes(
            								2.45, 10.0, 7.0, "cm", 
            								90.0, 0.10, "azsxdcfvgbh"), 
            					new ArrayList(Arrays.asList("ANS-011 Norma sanit�ria")), 
            					null, 
            					"Composi��o do produto", 
            					ProductCategory.getProductCategory(
            										0, 
            										"abcd", 
            										"Descri��o da categoria do produto")
            					, Arrays.asList(suplier));	
            System.out.println(pProduct.toString());
        }catch (Exception e) {
			System.out.println(e.getMessage());
		}
    }
}
