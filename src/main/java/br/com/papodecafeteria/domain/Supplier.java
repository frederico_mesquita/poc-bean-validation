package br.com.papodecafeteria.domain;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import br.com.papodecafeteria.domain.vldt.Vldt;

public class Supplier extends Vldt implements Serializable {
	private static final long serialVersionUID = 1L;
	private static Logger l = Logger.getLogger(Supplier.class.getName());
	
	public Supplier(){
		super();
	}
	
	private Supplier(int pKey, CNPJ pCNPJ, List<Product> pProducts){
		super();
		try{
			setiKey(pKey);
			setCnpj(pCNPJ);
			setProducts(pProducts);
		}catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
	}
	
	public static Supplier getSupplier(int pKey, CNPJ pCNPJ, List<Product> pProducts){
		Supplier pSupplier = null;
		try{
			pSupplier = new Supplier(pKey, pCNPJ, pProducts);
			validate(pSupplier);
		}catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return pSupplier;
	}
	
	@NotNull
	private int iKey;
	
	@NotNull @Valid
	private CNPJ cnpj;
	
	@NotNull @NotEmpty @Valid
	private List<Product> products;
	
	@Override
	public String toString(){
		String cReturn = "";
		try{
			cReturn += "key = [" + getiKey() + "], " +
					"cnpj = [" + getCnpj().toString() + "], " +
					"products = [" + toStringProducts() + "]";
		}catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return cReturn;
	}
	
	private String toStringProducts(){
		String cReturn = "";
		try{
			for(int iCount = 0; iCount < getProducts().size() - 1; iCount++)
				cReturn += getProducts().get(iCount) + ", ";
			cReturn += getProducts().get(getProducts().size() - 1);
		}catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return cReturn;
	}

	public int getiKey() {
		return iKey;
	}

	public void setiKey(int iKey) {
		this.iKey = iKey;
	}

	public CNPJ getCnpj() {
		return cnpj;
	}

	public void setCnpj(CNPJ cnpj) {
		this.cnpj = cnpj;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}
}
