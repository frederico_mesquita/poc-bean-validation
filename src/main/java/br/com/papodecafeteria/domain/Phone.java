package br.com.papodecafeteria.domain;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import br.com.papodecafeteria.domain.vldt.Vldt; 

public class Phone extends Vldt implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger l = Logger.getLogger(Phone.class.getName());
	
	public Phone(){
		super();
	}
	
	private Phone(int pKey, int pCodArea, String pPhoneNumber){
		super();
		try{
			setiKey(pKey);
			setiCodArea(pCodArea);
			setcCellPhoneNumber(pPhoneNumber);
		}catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
	}
	
	public static Phone getPhone(int pKey, int pCodArea, String pPhoneNumber){
		Phone pPhone = null;
		try{
			pPhone = new Phone(pKey, pCodArea, pPhoneNumber);
			validate(pPhone);
		}catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return pPhone;
	}
	
	@NotNull
	private int iKey;
	
	@NotNull @Min(11) @Max(99)
	private int iCodArea;
	
	@NotNull @Pattern(regexp="\\d{5}-\\d{4}")
	private String cCellPhoneNumber;
	
	@Override
	public String toString(){
		String cReturn = "";
		try{
			cReturn += "key = [" + getiKey() + "], " +
					"code area = [" + getiCodArea() + "], " + 
					"phone number = [" + getcCellPhoneNumber() + "]";
		}catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return cReturn;
	}

	public int getiKey() {
		return iKey;
	}
	public void setiKey(int iKey) {
		this.iKey = iKey;
	}
	public int getiCodArea() {
		return iCodArea;
	}
	public void setiCodArea(int iCodArea) {
		this.iCodArea = iCodArea;
	}
	public String getcCellPhoneNumber() {
		return cCellPhoneNumber;
	}
	public void setcCellPhoneNumber(String cPhoneNumber) {
		this.cCellPhoneNumber = cPhoneNumber;
	}
}
