package br.com.papodecafeteria.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.validation.Valid;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import br.com.papodecafeteria.domain.vldt.Vldt;

public class CNPJ extends Vldt implements Serializable {
	private static final long serialVersionUID = 1L;
	private static Logger l = Logger.getLogger(CNPJ.class.getName());
	
	public CNPJ(){
		super();
	}
	
	private CNPJ(int pKey, Date pCreationDate, String pName, String pFantasyName, 
			List<CNAE> pCNAE, List<CNAE> pEconomicSecondaryActs, NaturezaJuridicaCNPJ pNaturezaJuridicaCNPJ,
			List<ContactInformation> pContactInformation, String pCNPJ){
		super();
		try{
			setiKey(pKey);
			setdCreationDate(pCreationDate);
			setcName(pName);
			setcFantasyName(pFantasyName);
			setcCNAE(pCNAE);
			setcEconomicSecondaryActs(pEconomicSecondaryActs);
			setJuridicNature(pNaturezaJuridicaCNPJ);
			setContactInformation(pContactInformation);
			setcCNPJ(pCNPJ);
		}catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
	}
	
	public static CNPJ getCNPJ(int pKey, Date pCreationDate, String pName, String pFantasyName, 
			List<CNAE> pCNAE, List<CNAE> pEconomicSecondaryActs, NaturezaJuridicaCNPJ pNaturezaJuridicaCNPJ,
			List<ContactInformation> pContactInformation, String pCNPJ){
		CNPJ rCNPJ = null;
		try{
			rCNPJ = new CNPJ(pKey, pCreationDate, pName, pFantasyName, 
								pCNAE, pEconomicSecondaryActs, pNaturezaJuridicaCNPJ, 
								pContactInformation, pCNPJ);
			validate(rCNPJ);
		}catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return rCNPJ;
	}
	
	@NotNull
	private int iKey;
	
	@NotNull
	private Date dCreationDate;
	
	@NotNull @Size(min = 10, max = 200)
	private String cName;
	
	@NotNull @Size(min = 10, max = 200)
	private String cFantasyName;
	
	@NotNull @Valid
	private List<CNAE> cCNAE;
	
	@NotNull @Valid
	private List<CNAE> cEconomicSecondaryActs;
	
	@NotNull @Valid
	private NaturezaJuridicaCNPJ juridicNature;
	
	@NotNull @Valid
	private List<ContactInformation> contactInformation;
	
	@NotNull @Pattern(regexp="\\d{8}/\\d{4}-\\d{2}")
	private String cCNPJ;
	
	@Override
	public String toString(){
		String cReturn = "";
		try{
			cReturn += "key = [" + getiKey() + "], " +
					"creation date = [" + getdCreationDate().toString() + "], " +
					"name = [" + getcName() + "], " + 
					"fantasy name = [" + getcFantasyName() + "], " + 
					"cnae =[" + toStringCNAE() + "]," +
					"economic secondary acts =[" + toStringEconomicSecondaryActs() + "]," +
					"juridic nature =[" + getJuridicNature().toString() + "]," +
					"contact information =[" + getContactInformation().toString() + "], " +
					"cnpj =[" + toStringCNPJ()+ "]";
		}catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(),exc);
		}
		return cReturn;
	}
	
	private String toStringCNPJ(){
		String cReturn = "";
		try{
			cReturn += getcCNPJ().substring(0, 8) + "/" + getcCNPJ().substring(8, 12) + "-" + getcCNPJ().substring(12);
		}catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return cReturn;
	}
	
	private String toStringEconomicSecondaryActs(){
		String cReturn = "";
		try{
			for(int iCount = 0; iCount < getcEconomicSecondaryActs().size() - 1; iCount++)
				cReturn += getcEconomicSecondaryActs().get(iCount).toString() + ", ";
			cReturn += getcEconomicSecondaryActs().get(getcEconomicSecondaryActs().size() - 1);
		}catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return cReturn;
	}
	
	private String toStringCNAE(){
		String cReturn = "";
		try{
			for(int iCount = 0; iCount < getcCNAE().size() - 1; iCount++)
				cReturn += getcCNAE().get(iCount).toString() + ", ";
			cReturn += getcCNAE().get(getcCNAE().size() - 1);
		}catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return cReturn;
	}

	public Date getdCreationDate() {
		return dCreationDate;
	}
	public void setdCreationDate(Date dCreationDate) {
		this.dCreationDate = dCreationDate;
	}
	public String getcName() {
		return cName;
	}
	public void setcName(String cName) {
		this.cName = cName;
	}
	public String getcFantasyName() {
		return cFantasyName;
	}
	public void setcFantasyName(String cFantasyName) {
		this.cFantasyName = cFantasyName;
	}
	public List<CNAE> getcCNAE() {
		return cCNAE;
	}
	public void setcCNAE(List<CNAE> cCNAE) {
		this.cCNAE = cCNAE;
	}
	public List<CNAE> getcEconomicSecondaryActs() {
		return cEconomicSecondaryActs;
	}
	public void setcEconomicSecondaryActs(List<CNAE> cEconomicSecondaryActs) {
		this.cEconomicSecondaryActs = cEconomicSecondaryActs;
	}
	public List<ContactInformation> getContactInformation() {
		return contactInformation;
	}
	public void setContactInformation(List<ContactInformation> contactInformation) {
		this.contactInformation = contactInformation;
	}
	public String getcCNPJ() {
		return cCNPJ;
	}
	public void setcCNPJ(String cCNPJ) {
		this.cCNPJ = cCNPJ;
	}
	public int getiKey() {
		return iKey;
	}
	public void setiKey(int iKey) {
		this.iKey = iKey;
	}

	public NaturezaJuridicaCNPJ getJuridicNature() {
		return juridicNature;
	}

	public void setJuridicNature(NaturezaJuridicaCNPJ juridicNature) {
		this.juridicNature = juridicNature;
	}
	
}
