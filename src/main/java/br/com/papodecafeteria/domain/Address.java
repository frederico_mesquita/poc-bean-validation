package br.com.papodecafeteria.domain;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import br.com.papodecafeteria.domain.vldt.Vldt;

public class Address extends Vldt implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger l = Logger.getLogger(Address.class.getName());
	
	public Address(){
		super();
	}
	
	private Address(int pKey, String pStreet, String pIdentification, String pCEP, String pAdditionalInformation){
		super();
		try {
			setiKey(pKey);
			setcStreet(pStreet);
			setcIdentification(pIdentification);
			setcCEP(pCEP);
			setcAdditionalInformation(pAdditionalInformation);	
		}catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
	}
	
	public static Address getAddress(int pKey, String pStreet, String pIdentification, String pCEP, String pAdditionalInformation){
		Address pAddress = null;
		try{
			pAddress = new Address(pKey, pStreet, pIdentification, pCEP, pAdditionalInformation);
			validate(pAddress);
		}catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return pAddress;
	}
	
	@NotNull
	private int iKey;
	
	@NotNull @Pattern(regexp="\\d{5}-\\d{3}")
	private String cCEP;
	
	@Size(min=0, max=100)
	private String cStreet;
	
	@Size(min=1, max=10)
	private String cIdentification;
	
	@Size(min=0, max=100)
	private String cAdditionalInformation;
	
	@Override
	public String toString(){
		String cReturn = "";
		
		try {
			cReturn += "key = [" + getiKey() + "], " +
					"street = [" + getcStreet() + "], " +
					"identification = [" + getcIdentification() + "], " +
					"cep = [" + getcCEP() + "], " +
					"additional information = [" + getcAdditionalInformation() + "]";
		}catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		
		return cReturn;
	}

	public int getiKey() {
		return iKey;
	}
	public void setiKey(int iKey) {
		this.iKey = iKey;
	}
	public String getcCEP() {
		return cCEP;
	}
	public void setcCEP(String cCEP) {
		this.cCEP = cCEP;
	}
	public String getcAdditionalInformation() {
		return cAdditionalInformation;
	}
	public void setcAdditionalInformation(String cAdditionalInformation) {
		this.cAdditionalInformation = cAdditionalInformation;
	}
	public String getcStreet() {
		return cStreet;
	}
	public void setcStreet(String cStreet) {
		this.cStreet = cStreet;
	}
	public String getcIdentification() {
		return cIdentification;
	}
	public void setcIdentification(String cIdentification) {
		this.cIdentification = cIdentification;
	}
}
