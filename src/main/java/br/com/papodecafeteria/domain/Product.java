package br.com.papodecafeteria.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.papodecafeteria.domain.vldt.Vldt;

public class Product extends Vldt implements Serializable{
	private static final long serialVersionUID = 1L;
	private static Logger l = Logger.getLogger(Product.class.getName());
	
	public Product(){
		super();
	}
	
	private Product(int pKey, String pName, String pDescription, Dimensoes pDimensoes,
			List<String> pNormaSanitaria, List<String> pImagens, String pComposicao,
			ProductCategory pProductCategory, List<CNPJ> pSuppliers){
		super();
		try{
			setiKey(pKey);
			setcName(pName);
			setcDescription(pDescription);
			setDimensoes(pDimensoes);
			setcNormaSanitaria(pNormaSanitaria);
			setcImagens(pImagens);
			setcComposicao(pComposicao);
			setProductCategory(pProductCategory);
			setSuppliers(pSuppliers);
		}catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(),exc);
		}
	}
	
	public static Product getProduct(int pKey, String pName, String pDescription, Dimensoes pDimensoes,
			List<String> pNormaSanitaria, List<String> pImagens, String pComposicao,
			ProductCategory pProductCategory, List<CNPJ> pSuppliers){
		Product pProduct = null;
		try{
			pProduct = new Product(pKey, pName, pDescription, pDimensoes,
									pNormaSanitaria, pImagens, pComposicao, pProductCategory, pSuppliers);
			validate(pProduct);
		}catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(),exc);
		}
		return pProduct;
	}
	
	@NotNull
	private int iKey;
	
	@NotNull @Size(min = 10, max = 150)
	private String cName;
	
	@NotNull @Size(min = 10, max = 200)
	private String cDescription;
	
	@NotNull @Valid
	private Dimensoes dimensoes;
	
	@NotNull @NotEmpty
	private List<String> cNormaSanitaria = new ArrayList<String>();
	
	@NotNull @NotEmpty
	private List<String> cImagens = new ArrayList<String>();
	
	@NotNull @Size(min = 10, max = 300) 
	private String cComposicao;
	
	@NotNull @Valid
	private ProductCategory productCategory;
	
	@NotNull @Valid
	private List<CNPJ> suppliers = new ArrayList<CNPJ>();
	
	@Override
	public String toString(){
		String cReturn = "";
		try{
			cReturn += "key = [" + getiKey() + "], " +
					"name = [" + getcName() + "], " +
					"description = [" + getcDescription() + "], " +
					"dimensoes = [" + getDimensoes().toString() + "], " +
					"norma sanitaria = [" + toStringNormaSanitaria() + "], " +
					"imagens = [" + toStringImagens() + "], " +
					"composicao = [" + getcComposicao() + "], " +
					"product category = [" + getProductCategory().toString() + "], " +
					"suppliers = [" + toStringSuppliers() + "]";
		}catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(),exc);
		}
		return cReturn;
	}
	
	private String toStringSuppliers(){
		String cReturn = "";
		try{
			for(int iCount = 0; iCount < getSuppliers().size() - 1; iCount++)
				cReturn += getSuppliers().get(iCount).toString() + ", ";
			cReturn += getSuppliers().get(getSuppliers().size() - 1).toString();
		}catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return cReturn;
	}
	
	private String toStringImagens(){
		String cReturn = "";
		try{
			for(int iCount = 0; iCount < getcImagens().size() - 1; iCount++)
				cReturn += getcImagens().get(iCount) + ", ";
			cReturn += getcImagens().get(getcImagens().size() - 1);
		}catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return cReturn;
	}
	
	private String toStringNormaSanitaria(){
		String cReturn = "";
		try{
			for(int iCount = 0; iCount < getcNormaSanitaria().size() - 1; iCount++)
				cReturn += getcNormaSanitaria().get(iCount) + ", ";
			cReturn += getcNormaSanitaria().get(getcNormaSanitaria().size() - 1);
		}catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return cReturn;
	}

	public int getiKey() {
		return iKey;
	}
	public void setiKey(int iKey) {
		this.iKey = iKey;
	}
	public String getcName() {
		return cName;
	}
	public void setcName(String cName) {
		this.cName = cName;
	}
	public String getcDescription() {
		return cDescription;
	}
	public void setcDescription(String cDescription) {
		this.cDescription = cDescription;
	}
	public Dimensoes getDimensoes() {
		return dimensoes;
	}
	public void setDimensoes(Dimensoes dimensoes) {
		this.dimensoes = dimensoes;
	}
	
	public List<String> getcNormaSanitaria() {
		return cNormaSanitaria;
	}
	public void setcNormaSanitaria(List<String> cNormaSanitaria) {
		this.cNormaSanitaria = cNormaSanitaria;
	}
	public void addcNormaSanitaria(String pNormaSanitaria) {
		getcNormaSanitaria().add(pNormaSanitaria);
	}
	public void removecNormaSanitaria(String pNormaSanitaria) {
		getcNormaSanitaria().remove(pNormaSanitaria);
	}
	
	public List<String> getcImagens() {
		return cImagens;
	}
	public void setcImagens(List<String> cImagens) {
		this.cImagens = cImagens;
	}
	public void addcImagens(String pImagen) {
		getcImagens().add(pImagen);
	}
	public void removecImagens(String pImagen) {
		getcImagens().remove(pImagen);
	}
	
	public String getcComposicao() {
		return cComposicao;
	}
	public void setcComposicao(String cComposicao) {
		this.cComposicao = cComposicao;
	}
	public ProductCategory getProductCategory() {
		return productCategory;
	}
	public void setProductCategory(ProductCategory productCategory) {
		this.productCategory = productCategory;
	}
	
	public List<CNPJ> getSuppliers() {
		return suppliers;
	}
	public void setSuppliers(List<CNPJ> suppliers) {
		this.suppliers = suppliers;
	}
	public void addSuppliers(CNPJ pSupplier) {
		getSuppliers().add(pSupplier);
	}
	public void removeSuppliers(CNPJ pSupplier) {
		getSuppliers().remove(pSupplier);
	}
}
