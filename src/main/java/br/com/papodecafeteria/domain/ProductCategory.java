package br.com.papodecafeteria.domain;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import br.com.papodecafeteria.domain.vldt.Vldt;

public class ProductCategory extends Vldt implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger l = Logger.getLogger(ProductCategory.class.getName());
	
	public ProductCategory(){
		super();
	}
	
	private ProductCategory(int pKey, String pCode, String pDescription){
		super();
		try{
			setiKey(pKey);
			setcCode(pCode);
			setcDescription(pDescription);
		}catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
	}
	
	public static ProductCategory getProductCategory(int pKey, String pCode, String pDescription){
		ProductCategory pProductCategory = null;
		try{
			pProductCategory = new ProductCategory(pKey, pCode, pDescription);
			validate(pProductCategory);
		}catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return pProductCategory;
	}
	
	@NotNull
	private int iKey;
	
	@NotNull @Pattern(regexp = "^[\\p{Alnum}]{1,3}$")
	private String cCode;
	
	@NotNull @Size(min = 10, max = 150)
	private String cDescription;
	
	@Override
	public String toString(){
		String cReturn = "";
		try{
			cReturn += "key = [" + getiKey() + "], " +
					"code = [" + getcCode() + "], " +
					"description = [" + getcDescription() + "]";
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return cReturn;
	}

	public int getiKey() {
		return iKey;
	}
	public void setiKey(int iKey) {
		this.iKey = iKey;
	}
	public String getcCode() {
		return cCode;
	}
	public void setcCode(String cCode) {
		this.cCode = cCode;
	}
	public String getcDescription() {
		return cDescription;
	}
	public void setcDescription(String cDescription) {
		this.cDescription = cDescription;
	}
}
